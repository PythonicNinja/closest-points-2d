# -*- coding: utf-8 -*-
# CREATED ON DATE: 10.03.15
__author__ = 'vojtek.nowak@gmail.com'

import os
import re
import random
import argparse

PREFIX = 'input'
EXTENSION = 'txt'
MIN = -1000000
MAX = 1000000

INPUT_FOLDER = os.path.join(os.path.curdir, 'input')

def generate_file(file_name, arguments):
    with file(os.path.join(INPUT_FOLDER, file_name), 'w') as generated_file:
        for i in range(arguments.count):
            generated_point = map(str, [random.randint(MIN, MAX), random.randint(MIN, MAX)])
            generated_file.write(" ".join(generated_point) + "\n")


def generate_files(arguments):
    files = int(arguments.files)

    last_file = int(re.findall(r'^'+PREFIX+r'([0-9]+)\.'+EXTENSION, os.listdir(INPUT_FOLDER)[-1])[-1])

    for i in range(last_file, last_file+files):
        generate_file(file_name=PREFIX+str(i+1)+'.'+EXTENSION, arguments=arguments)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generate points for closest points.')
    parser.add_argument("count", help="number of points in file", type=int, default=10)
    parser.add_argument("files", help="number of files", type=int, default=1)

    args = parser.parse_args()

    generate_files(arguments=args)