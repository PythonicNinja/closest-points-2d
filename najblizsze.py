# -*- coding: utf-8 -*-
# CREATED ON DATE: 24.02.15
__author__ = 'vojtek.nowak@gmail.com'

import os
import math
from timeit import default_timer as timer

#################################################################################################
# Zaimplementuj omawiany na wykładzie algorytm wyznaczania
# pary najbliższych punktów metodą „dziel i zwyciężaj”.
#
# Wejście. Pary liczb (całkowitych) kodujących punkty na płaszczyźnie zapisane w kolejnych
# wierszach pliku tekstowego.
#
# Wyjście. Para najbliższych punktów.
#################################################################################################
# Wstępne przetwarzanie. wykonywane tylko raz./
# Sx – posortowany zbiór S punktów wejściowych względem współrzędnej x. Sy – posortowany zbiór S punktów wejściowych
# względem współrzędnej y. Zbiór wejściowy S reprezentowany jest przez parę (Sx, Sy).

# Idea algorytmu /opartego na metodzie „dziel i zwyciężaj”/

# 1. Podziel zbiór S na dwa (prawie) równoliczne podzbiory S1 i S2 (wraz z odpowiednią
# reprezentacją przez pary uporządkowanych zbiorów) mające tę własność,
# że dla wszystkich p ∈ S1 i q ∈ S2 zachodzi p ≺ q. Niech ℓ będzie pionową prostą przechodzącą przez największy punkt w S1.

# 2. Rozwiąż rekurencyjnie problem w S1 i S2. Niech {p1, p2} i {q1, q2} będą rozwiązaniami
# problemu w S1 i w S2. δ := min{dE(p1, p2),(dE(q1, q2)}.

# 3. Niech S1(δ, ℓ) będzie zbiorem tych punktów z S1, które znajdują się nie dalej
# niż δ od ℓ; analogicznie, niech S2(δ, ℓ) będzie zbiorem tych punktów z S2, które
# znajdują się nie dalej niż δ od prostej ℓ. Wyznacz parę (p3, q3) najbliższych punktów w S3 = S1(δ, ℓ) × S2(δ, ℓ). Niech δ3 będzie odległością punktów p3 i q3.

# 4. Zwróć parę punktów realizującą min{δ, δ3}.

# NAJMNIEJ-ODLEGŁA-PARA(Q, X, Y)
# 1  if |Q|=2 then return \infty
# 2  if |Q|=2 then return |Q[1] - Q[2]|
# 3  korzystając z tablicy X znajdź pionową prostą l taką, że po jej lewej i
# prawej stronie jest \lfloor \frac{|Q|}{2} \rfloor punktów
# 4  niech Q_L  i Q_P to będą punkty odpowiednio po lewej i prawej stronie l
# 5  wyznacz tablice X_L,X_P oraz Y_L,Y_P
# 6  d_L =NAJMNIEJ-ODLEGŁA-PARA(Q_L, X_L, Y_L)
# 7  d_R =NAJMNIEJ-ODLEGŁA-PARA(Q_R, X_R, Y_R)
# 8  d = \min(d_L, d_R)
# 9  niech Y' będzie tablicą Y po usunięciu z niej punktów odległych od l o więcej niż d
# 10 for i=1 to |Y'| do
# 11   for j=1 to \min(7,|Y'|-i) do
# 12     if |P[i] - P[i+j]| < d then d=|P[i]-P[i+j]|
# 13 return d

INFINITY = 9999999999999999999
INPUT_FOLDER = os.path.join(os.path.curdir, 'input')


class Point:
    def __init__(self, x=None, y=None):
        self.x = x
        self.y = y

    def __repr__(self):
        return "{%(x)s,%(y)s}" % ({'x': self.x, 'y': self.y})

    @classmethod
    def dist(cls, p1, p2):
        distance = math.sqrt((p2.x - p1.x) ** 2 + (p2.y - p1.y) ** 2)
        return distance

    @classmethod
    def print_distance(cls, p1, p2):
        d = cls.dist(p1, p2)
        print 'DISTANCE %(p1)s -> %(p2)s = %(distance)s' % ({'p1': p1, 'p2': p2, 'distance': d})
        return d


def prepare(input_file):
    points = []
    for line in input_file.readlines():
        points.append(Point(*map(int, line.replace('\n', '').split(' '))))

    sx = sorted(points, key=lambda point: point.x)
    sy = sorted(points, key=lambda point: point.y)

    # print
    # print 'SX:', sx
    # print 'SY:', sy
    # print

    return sx, sy


def partition(sx, sy):
    m = len(sx) / 2
    part1, part2 = (sx[:m], []), (sx[m:], [])

    for s_y in sy:
        found = False
        for s_x in part1[0]:
            if s_x.x == s_y.x:
                part1[1].append(s_y)
                found = True
        if not found:
            part2[1].append(s_y)

    # print
    # print 'PART1:', part1
    # print 'PART2:', part2
    # print

    return part1, part2, m


def closest_points(sx, sy, end_function):
    if len(sx) == 2:
        return Point.dist(sx[0], sx[1])
    if len(sx) < 2:
        return INFINITY

    p1, p2, m = partition(sx, sy)

    dl = closest_points(sx=p1[0], sy=p1[1], end_function=end_function)
    dr = closest_points(sx=p2[0], sy=p2[1], end_function=end_function)

    d = min([dl, dr])

    return end_function(m, d, sx, sy, p1, p2)


def closest_points_naive(sx):
    d = INFINITY
    for x1 in sx:
        for x2 in sx:
            if x1 != x2:
                distance = Point.dist(x1, x2)
                if distance < d:
                    d = distance

    return d


def end_function(m, d, sx, sy, p1, p2):
    # sy = [y for y in sy if m - d < y.x or m + d < y.x]
    # niech Y' będzie tablicą Y po usunięciu z niej punktów odległych od l o więcej niż d
    # for i=1 to |Y'| do
    # for j=1 to \min(7,|Y'|-i) do
    # if |P[i] - P[i+j]| < d then d=|P[i]-P[i+j]|
    # return d

    for i in xrange(1, len(sy)):
        for j in xrange(i + 1, min(7, len(sy))):
            if sy[j].y - sy[i].y >= d:
                break
            if sy[i] != sy[j]:
                distance = Point.dist(sy[i], sy[j])
                if distance < d:
                    d = distance
    return d


def end_function2(m, d, sx, sy, p1, p2):
    last_j = 0
    for i in xrange(0, len(p1[1])):
        for j in xrange(last_j, min(7, len(p2[1]))):
            if p2[1][j].y - p1[1][i].y >= d:
                last_j = j
                break
        for j in xrange(last_j, min(7, len(p2[1]))):
            if p1[1][i].y - p2[1][j].y >= d:
                break
            distance = Point.dist(p1[1][i], p2[1][j])
            if distance < d:
                d = distance
    return d

if __name__ == '__main__':
    input_files = os.listdir(INPUT_FOLDER)

    print 'FUNCTION:\t\t\t\t%(f1)s\t\t\t\t%(f2)s\t\t\t\t%(f3)s' % (
        {'f1': 'end_function', 'f2': 'end_function2', 'f3': 'naive'})
    for input_file_name in input_files:
        with open(os.path.join(INPUT_FOLDER, input_file_name)) as input_file:

            sx, sy = prepare(input_file)

            start1 = timer()
            d1 = closest_points(sx, sy, end_function=end_function)
            end1 = timer()

            start2 = timer()
            d2 = closest_points(sx, sy, end_function=end_function2)
            end2 = timer()

            start3 = timer()
            d3 = closest_points_naive(sx)
            end3 = timer()

            if d1 == d2:
                d_separator = '='
            else:
                d_separator = '!='

            if (end1 - start1) <= (end2 - start2):
                time_separator = '<'
            else:
                time_separator = '>'

            print '-' * 16 + '+' + '-' * 27 + '+' + '-' * 25 * 2
            print 'FILE:\t\t\t|\t\t', input_file_name, '\t\t\t\t\t' + str(len(sy)) + '\t\t'
            print 'DISTANCE:\t\t|\t\t%(d1)1.8f\t\t%(d_separator)s\t\t%(d2)1.8f\t\t\t%(d3)1.8f' \
                  % ({
                     'd1': d1,
                     'd_separator': d_separator,
                     'd2': d2,
                     'd3': d3
                     })
            print 'TIME:\t\t\t|\t\t%(t1)4.8f\t\t\t%(time_separator)s\t\t%(t2)4.8f\t\t\t\t%(t3)4.8f' \
                  % ({
                     't1': (end1 - start1),
                     'time_separator': time_separator,
                     't2': (end2 - start2),
                     't3': (end3 - start3)
                     })

